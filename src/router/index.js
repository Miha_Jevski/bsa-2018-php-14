import Vue from 'vue'
import Router from 'vue-router'
import UsersList from '@/components/UsersList'
import User from '@/components/User'
import UserProfile from '@/components/UserProfile'
import AlbumsList from '@/components/AlbumsList'
import E404 from '@/components/E404'


Vue.use(Router)

export default new Router({
    base: '/',
    linkActiveClass: 'active',
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: {name: 'users'}
        },
        {
            path: '/users',
            name: 'users',
            component: UsersList,
            children: [
                // an empty path will be treated as the default, e.g.
                // components rendered at /parent: Root -> Parent -> Default
                // { path: '', component: UsersList },

                // components rendered at /parent/foo: Root -> Parent -> Foo
                {
                    path: 'add',
                    name: 'add-user',
                    component: AlbumsList }
            ]
        },
        {
            path: '/users/:id',
            component: User,
            children: [
                { path: 'edit', component: UserProfile },
            ]
        },
        {
            path: '/albums',
            name: 'albums',
            component: AlbumsList
        },
        {
            path: '*',
            component: E404
        }
    ]
})
