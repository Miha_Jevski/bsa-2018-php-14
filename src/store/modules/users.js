import Vue from 'vue';
import data from '../../api/user-data.js'

let lastId = 10; //todo

export default {
	namespaced: true,
	state: {
        users: []
	},
	getters: {
		users(state){
			return state.users;
		},
        usersMap(state){
            let usersMap = {};

            for(let i = 0; i < state.users.length; i++){
                let user = state.users[i];
                usersMap[user.id] = user;
            }

            return usersMap;
        },
        user: (state, getters) => (id) => {
            return getters.usersMap[id];
        }
	},
	mutations: {
        LOAD_USERS(state, users) {
            state.users = users
        },

        ADD_USER(state, user) {
            lastId++;

            state.users.push({
                id: lastId,
                name: user.name,
                email: user.email,
                avatar: user.avatar
            });
        },

        EDIT_USER(state, { userId, data }) {
            const ind = state.users.findIndex(user => user.id === userId);

            if (ind !== -1) {
                const updatedUser = {
                    id: userId,
                    name: data.name,
                    email: data.email,
                    avatar: data.avatar
                };

                Vue.set(state.users, ind, updatedUser);
            }
        },

        DELETE_USER(state, userId) {
            const ind = state.users.findIndex(user => user.id === userId);

            if (ind !== -1) {
                state.users.splice(ind, 1);
            }
        }
	},
	actions: {
        loadUsers({ commit }) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('LOAD_USERS', data);
                    resolve();
                }, 250);
            });
        },

        addUser({ state, commit, rootState }, data) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('ADD_USER', data);
                    resolve();
                }, 250);
            });
        },

        editUser({ commit }, data) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('EDIT_USER', data);
                    resolve();
                }, 250);
            });
        },

        deleteUser({ commit }, userId) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('DELETE_USER', userId);
                    resolve();
                }, 250);
            });
        }
	}
};