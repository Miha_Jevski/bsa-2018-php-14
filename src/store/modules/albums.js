import Vue from 'vue';
import data from '../../api/albums-data.js'

let lastId = 100; //todo

export default {
    namespaced: true,
    state: {
        albums: []
    },
    getters: {
        albums(state){
            return state.albums;
        },
        albumsMap(state){
            let albumsMap = {};

            for(let i = 0; i < state.albums.length; i++){
                let album = state.albums[i];
                albumsMap[album.id] = album;
            }

            return albumsMap;
        },
        album: (state, getters) => (id) => {
            return getters.albumsMap[id];
        }
    },
    mutations: {
        LOAD_albums(state, albums) {
            state.albums = albums
        },

        ADD_ALBUM(state, album) {
            lastId++;

            state.albums.push({
                id: lastId,
                title: album.title,
                previe: album.previe,
                user_id: album.user_id
            });
        },

        EDIT_ALBUM(state, { albumId, data }) {
            const ind = state.albums.findIndex(album => album.id === albumId);

            if (ind !== -1) {
                const updatedAlbum = {
                    id: albumId,
                    title: album.title,
                    previe: album.previe,
                    user_id: album.user_id
                };

                Vue.set(state.albums, ind, updatedAlbum);
            }
        },

        DELETE_ALBUM(state, albumId) {
            const ind = state.albums.findIndex(album => album.id === albumId);

            if (ind !== -1) {
                state.albums.splice(ind, 1);
            }
        }
    },
    actions: {
        loadAlbums({ commit }) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('LOAD_ALBUM', data);
                    resolve();
                }, 250);
            });
        },

        addAlbum({ state, commit, rootState }, data) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('ADD_ALBUM', data);
                    resolve();
                }, 250);
            });
        },

        editAlbum({ commit }, data) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('EDIT_ALBUM', data);
                    resolve();
                }, 250);
            });
        },

        deleteAlbum({ commit }, albumId) {
            return new Promise(resolve => {
                setTimeout(() => {
                    commit('DELETE_ALBUM', albumId);
                    resolve();
                }, 250);
            });
        }
    }
};