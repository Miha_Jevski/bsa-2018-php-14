import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import users from './modules/users';
import albums from './modules/albums';

export default new Vuex.Store({
    modules: {
        users,
        albums,
    },
    strict: true
});